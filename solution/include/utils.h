//
// Created by vaporplatinate on 12/31/23.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_UTILS_H
#define ASSIGNMENT_5_SEPIA_FILTER_UTILS_H

#include "bmp_proc.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum status {
    OK = 0,
    ERROR_OPEN,
    ERROR_CLOSE,
    ERROR_CANT_SAVE,
    ERROR_ARGS_COUNT,
    ERROR_SIGNATURE,
    ERROR_HEADER,
    ERROR_INVALID_BITS,
    ERROR_INVALID_BIT_COUNT
};
struct input_data {
    enum status status_code;
    FILE* in;
    FILE* out;
};
struct input_data parse_input(char** argv);
enum status print_error(enum status stat);
enum status close_file(FILE* file);
bool angle_is_valid(const char* angle);

#endif //ASSIGNMENT_5_SEPIA_FILTER_UTILS_H
