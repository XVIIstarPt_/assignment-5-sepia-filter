//
// Created by vaporplatinate on 1/5/24.
//

#include "image.h"

#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H

void sepia_filter(struct image* src);

#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H
