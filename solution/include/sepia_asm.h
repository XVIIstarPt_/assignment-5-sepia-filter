//
// Created by vaporplatinate on 1/5/24.
//

#include "image.h"

#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_ASM_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_ASM_H

void sepia_asm(struct image* src);

#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_ASM_H
