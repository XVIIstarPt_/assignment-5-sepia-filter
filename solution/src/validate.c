//
// Created by vaporplatinate on 1/5/24.
//
#include "../include/ANSI_colors.h"
#include "../include/utils.h"

#include <stdio.h>
const char *error_messages[] = {
        GREENHB "COMPLETE" RESET, // OK
        REDHB "Failed to open file\n" RESET, // ERROR_OPEN
        REDHB "Failed to close file\n" RESET, // ERROR_CLOSE
        REDHB "Failed to save image\n" RESET, // ERROR_CANT_SAVE
        REDHB "Not enough arguments\n" RESET, // ERROR_ARGS_COUNT
        REDHB "Failed to rotate: Cannot parse angle\n" RESET, // ERROR_CANT_PARSE_ANGLE
        REDHB "Failed to rotate: Angle should be in [0, 90, -90, 180, -180, 270, -270]\n" RESET, // ERROR_BAD_ANGLE
        REDHB "Failed to load image: Invalid signature\n" RESET, // ERROR_SIGNATURE
        REDHB "Failed to load image: Invalid header\n" RESET, // ERROR_HEADER
        REDHB "Failed to load image: Invalid bits\n" RESET, // ERROR_INVALID_BITS
        REDHB "Failed to load image: Bit count is not supported\n" RESET, // ERROR_INVALID_BIT_COUNT
};

enum status print_error(enum status s) {
    printf("%s", error_messages[s]);
    return s;
}

enum status close_file(FILE* file) {
    if (fclose(file) != 0) {
        return print_error(ERROR_CLOSE);
    }
    return OK;
}

struct input_data parse_input(char** argv) {
    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");

    if (in == NULL || out == NULL) {
        return (struct input_data) { .status_code = ERROR_OPEN };
    }

    return (struct input_data) {
            .status_code = OK,
            .in = in,
            .out = out,
    };
}
