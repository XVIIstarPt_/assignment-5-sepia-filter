//
// Created by vaporplatinate on 12/31/23.
//

#include "../include/image.h"
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };
}
void delete_image(struct image* img){
    free(img -> data);
}
