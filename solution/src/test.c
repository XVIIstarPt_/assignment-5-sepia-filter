//
// Created by vaporplatinate on 1/5/24.
//
#include "../include/ANSI_colors.h"
#include "../include/bmp_proc.h"
#include "../include/sepia_asm.h"
#include "../include/sepia_filter.h"
#include "../include/utils.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
//#include <windows.h>
enum status read_bmp(FILE* in, struct image* img) {
    enum read_status rs = from_bmp(in, img);
    if (rs != READ_OK) {
        enum status status_to_message[] = {
                OK,ERROR_SIGNATURE,ERROR_INVALID_BITS,ERROR_HEADER,ERROR_INVALID_BIT_COUNT
        };
        return print_error(status_to_message[rs]);
    }
    return close_file(in);
}

enum status write_bmp(FILE* out, const struct image* img) {
    enum write_status ws = to_bmp(out, img);
    if (ws != WRITE_OK) {
        close_file(out);
        return print_error(ERROR_CANT_SAVE);
    }
    return close_file(out);
}

void my_memcpy(void *dest, const void *src, size_t n) {
    for (size_t i = 0; i < n; i++) {
        ((char*)dest)[i] = ((char*)src)[i];
    }
}

struct image clone_image(const struct image *img) {
    struct image copy =  create_image(img->width, img->height);
    my_memcpy(copy.data, img->data, img->width * img->height * sizeof(struct pixel));
    return copy;
}

double benchmark_sepia(char* filepath, uint16_t runs, bool use_simd) {
    struct image img;
    enum read_status rs = from_bmp(fopen(filepath, "rb"), &img);
    if (rs != READ_OK) {
        printf(REDHB "No such file.\n" RESET);
        exit(rs);
    }

    double elapsed_time = 0;
    clock_t start_time, end_time;
    for (uint16_t run = 0; run < runs; run++) {
        struct image cloned_image = clone_image(&img);

        if (use_simd) {
            start_time = clock();
            sepia_asm(&cloned_image);
            end_time = clock();
        } else {
            start_time = clock();
            sepia_filter(&cloned_image);
            end_time = clock();
        }

        elapsed_time += end_time - start_time;
        free(cloned_image.data);
    }

    delete_image(&img);
    return elapsed_time / CLOCKS_PER_SEC;
}

int main(void) {
    printf(REDB UWHITE "Smaller file (1.28 MiB, tfp_soundwave), 100 times, without SIMD (c):  %f seconds", benchmark_sepia("../memes/test1.bmp", 100, false));
    printf(RESET "\n");
    printf(REDB UWHITE "Smaller file (1.28 MiB, tfp_soundwave), 100 times, with SIMD  (asm):  %f seconds", benchmark_sepia("../memes/test1.bmp", 100, true));

    printf(RESET "\n\n");

    printf(MAGENTAB UBLACK "Smaller file (1.28 MiB, tfp_soundwave), 1000 times, without SIMD (c): %f seconds", benchmark_sepia("../memes/test1.bmp", 1000, false));
    printf(RESET "\n");
    printf(MAGENTAB UBLACK "Smaller file (1.28 MiB, tfp_soundwave), 1000 times, with SIMD  (asm): %f seconds", benchmark_sepia("../memes/test1.bmp", 1000, true));

    printf(RESET "\n\n");

    printf(REDB UYELLOW "Larger file (6.91 MiB, Anfield), 100 times, without SIMD (c):         %f seconds", benchmark_sepia("../memes/test2.bmp", 100, false));
    printf(RESET "\n");
    printf(REDB UYELLOW "Larger file (6.91 MiB, Anfield), 100 times, with SIMD  (asm):         %f seconds", benchmark_sepia("../memes/test2.bmp", 100, true));
    printf(RESET "\n\n");
    return 0;
}
