//
// Created by vaporplatinate on 12/31/23.
//
#include "../include/image.h"
#include "../include/sepia_asm.h"
#include "../include/sepia_filter.h"

//Sepia filter pixel color coefficients
static const float coef[3][3] = {
        {0.272f, 0.534f, 0.131f},
        {0.349f, 0.686f, 0.168f},
        {0.393f, 0.769f, 0.189f}
};
//Sepia filter saturation
static inline uint8_t defSaturation(uint64_t x) {
    return (x >> 8) ? 255 : x;
}

static void sepia_pixel(struct pixel* pixel){
    const struct pixel input = *pixel;
    pixel -> r = defSaturation(input.r * coef[0][0] + input.g * coef[0][1] + input.b * coef[0][2]);
    pixel -> g = defSaturation(input.r * coef[1][0] + input.g * coef[1][1] + input.b * coef[1][2]);
    pixel -> b = defSaturation(input.r * coef[2][0] + input.g * coef[2][1] + input.b * coef[2][2]);
}

void sepia_filter(struct image* src) {
    uint64_t pixels_total = src -> height * src -> width;
    for(uint64_t i = 0; i < pixels_total; i++){
        sepia_pixel(src -> data + i);
    }
}
