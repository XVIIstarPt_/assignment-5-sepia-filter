//
// Created by vaporplatinate on 1/5/24.
//
#include "../include/ANSI_colors.h"
#include "../include/bmp_proc.h"
#include "../include/sepia_filter.h"
#include "../include/utils.h"

#include <stdio.h>

#ifdef ASMSEPIA
    #include "../include/sepia_asm.h"
#endif

#define ARGS_NUMBER 3

enum status read_bmp(FILE* in, struct image* img) {
    enum read_status rs = from_bmp(in, img);
    if (rs != READ_OK) {
        enum status status_to_message[] = {
                OK,ERROR_SIGNATURE,ERROR_INVALID_BITS,ERROR_HEADER,ERROR_INVALID_BIT_COUNT
        };
        return print_error(status_to_message[rs]);
    }
    return close_file(in);
}

enum status write_bmp(FILE* out, const struct image* img) {
    enum write_status ws = to_bmp(out, img);
    if (ws != WRITE_OK) {
        close_file(out);
        return print_error(ERROR_CANT_SAVE);
    }
    return close_file(out);
}

int main(int argc, char** argv) {
    if (argc != ARGS_NUMBER) return print_error(ERROR_ARGS_COUNT);

    struct input_data input = parse_input(argv);
    if (input.status_code != 0) {
        return print_error(input.status_code);
    }
    struct image img;
    enum status rs = read_bmp(input.in, &img);
    if (rs != OK) return rs;

    printf(CYANHB "Image loaded\n" RESET);
#ifdef ASMSEPIA
    sepia_asm(&img);
    printf(GREENHB "Sepia filtering (ASM) done!\n" RESET);
#else
    sepia_filter(&img);
    printf(GREENB "Sepia filtering done!\n" RESET);
#endif
    enum status ws = write_bmp(input.out, &img);
    if (ws == OK) printf(BHGREEN "Image saved\n" RESET);

    delete_image(&img);
    return ws;
}
