; Coefficients for sepia filter
section .rodata
    coef_r: dd 0.272, 0.534, 0.131, 0.0
    coef_g: dd 0.349, 0.686, 0.168, 0.0
    coef_b: dd 0.393, 0.769, 0.189, 0.0

section .text

%macro sepia_for_color 2
  movdqu xmm4, xmm3
  mulps  xmm4, %1
  haddps xmm4, xmm4
  haddps xmm4, xmm4
  cvtps2dq xmm4, xmm4
  extractps rax, xmm4, 0
  cmp rax, rdx
  cmovg rax, rdx
  mov byte[rdi + %2], al
%endmacro

global sepia_asm

sepia_asm:
  mov rsi, [rdi]
  imul rsi, [rdi + 8]
  imul rsi, 3
  mov rdi, [rdi + 16]
  add rsi, rdi

  movdqu xmm2, [rel coef_r] ; <- mask for getting r
  movdqu xmm1, [rel coef_g] ; <- mask for getting g
  movdqu xmm0, [rel coef_b] ; <- mask for getting b

  .loop:
    movdqu xmm3, [rdi]

    pmovzxbd xmm3, xmm3
    cvtdq2ps xmm3, xmm3
    mov rdx, 0xff
    sepia_for_color xmm2, 2
    sepia_for_color xmm1, 1
    sepia_for_color xmm0, 0

    add rdi, 3
    cmp rdi, rsi
    jl .loop

  ret
